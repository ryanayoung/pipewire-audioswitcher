# pipewire audioswitcher



## Description:

A simple bash script to cycle through pipewire audio devices.

## Use:

* Download and place somewhere you keep your scripts.
* Make executable.
* Execute script as desired.
  * I like to set a keyboard shortcut in KDE to be able to cycle devices. I use Shift + Mute.
  * Could also add it as an alias in your .bashrc