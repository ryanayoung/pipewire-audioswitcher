#!/bin/sh

# Title: Pulse Audio Switcher
# Author: Ryan Young
# Description:
# Cycles through each connected audio device. 
# If you are using easyeffects it will skip over 
# that so it doesn't set easyeffects as your default device. 
# Make this file executable and then attach it to a hotkey 
# in your DE or put it in your path somewhere.

#get all devices
delete_var="Name:"
SINKS=$(pactl list sinks | grep -e "Name:")
SINKS=($SINKS)
SINKS=( ${SINKS[@]/$delete_var})
SINK_WIDTH=${#SINKS[@]}
#echo $SINK_WIDTH

#get current device
curr_device="$(pactl get-default-sink)"

#iterate through devices
for i in "${!SINKS[@]}"
do
   echo $i
   #set device to next in device list
   if [ "$curr_device" = "${SINKS[$i]}" ];
   then
      next=$(($i + 1))

      #handle array overflow
      if [ $next -ge $SINK_WIDTH ];
      then
         next=0
      fi

      #do not set easyeffects as default device
      if [ ${SINKS[$next]} = "easyeffects_sink" ];
      then
         next=$(($i+2))

         #recheck array overflow
         if [ $next -ge $SINK_WIDTH ];
         then
            next=0
         fi
      fi
      pactl set-default-sink ${SINKS[$next]}
      break
   fi
done
